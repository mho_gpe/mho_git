*** Settings ***
Resource    squash_resources.resource
Library    SeleniumLibrary

    


*** Test Cases ***
Connexion à l'application

    [Setup]	Test Setup

    Given l'utilisateur est sur la page d'accueil
	When l'utilisateur souhaite prendre rendez-vous
	Then la page de connexion s'affiche
	When l'utilisateur se connecte
	Then l'utilisateur est connecté sur la page de rendez-vous

	[Teardown]	Test Teardown
